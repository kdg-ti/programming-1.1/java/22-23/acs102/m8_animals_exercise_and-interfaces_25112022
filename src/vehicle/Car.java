package vehicle;

import geolocation.Location;
import geolocation.MovementTracker;
import geolocation.Tracked;

public class Car implements Tracked {
  private String brand;
  private int cubicCapacity;
  private int horsePower;
  private Location point;

  public Car(String brand, int cc, int horsePower) {
    point=new Location(0.0,0.0);
    this.brand = brand;
    this.cubicCapacity = cc;
    this.horsePower = horsePower;
  }

  @Override
  public String toString() {
    return "Car{" +
        "brand='" + brand + '\'' +
        ", cc=" + cubicCapacity +
        ", horsePower=" + horsePower +
        ", location: [" + getLocation() +
        "] }";
  }

  @Override
  public Location getLocation() {
    return point;
  }

  @Override
  public void setLocation(Location point) {
    this.point=point;
  }
}
