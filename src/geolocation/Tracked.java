package geolocation;

public interface Tracked {


    Location getLocation() ;

    void  setLocation(Location point) ;
}
