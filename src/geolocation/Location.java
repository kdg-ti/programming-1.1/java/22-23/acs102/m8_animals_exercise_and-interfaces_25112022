package geolocation;

public class Location {
  double lattitude;
  double longitude;

  public Location(double lattitude, double longitude) {
    this.lattitude = lattitude;
    this.longitude = longitude;
  }

  @Override
  public String toString() {
    return "lattitude=" + lattitude +
        ", longitude=" + longitude ;
  }
}
