package geolocation;

public interface MovementTracker extends Tracked{
  double getSpeed();
}
