package animals;

import geolocation.Location;
import geolocation.Tracked;

public class Dog extends Animal implements Tracked {
  private static int biggestChipNumber;
  private final int chipNumber;
  private Location point = new Location(51.22553144647569, 4.436250377320662);

  public static int getBiggestChipNumber() {
    return biggestChipNumber;
  }

  public Dog(String name, String breed, String colour, int chipNumber) {
    super(name, breed, colour);
    if (chipNumber <= biggestChipNumber){
      biggestChipNumber++;
      chipNumber = biggestChipNumber;
    } else {
      biggestChipNumber = chipNumber;
    }
    this.chipNumber = chipNumber;
  }

  public Dog(String name, String breed, String colour) {
    this(name, breed, colour, -1);
  }

  public int getChipNumber() {
    return chipNumber;
  }

  @Override
  public String getTagLine() {
    return "Like a dog in a bowling game";
  }

  @Override
  public String toString() {
    return String.format("%s with chipnumber %s is at %s", super.toString(), getChipNumber(),getLocation());
  }

  @Override
  public Location getLocation() {
    return point;
  }

  @Override
  public void setLocation(Location point) {
    this.point = point;
  }
}
