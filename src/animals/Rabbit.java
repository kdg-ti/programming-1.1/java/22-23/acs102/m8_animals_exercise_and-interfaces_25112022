package animals;

public class Rabbit extends Animal{
  private boolean digs;

  public Rabbit(String name, String breed, String colour,boolean digs) {
    super(name, breed, colour);
    this.digs=digs;
  }

  public boolean getDigs() {
    return digs;
  }

  @Override
  public String getTagLine() {
    return "Carrots are good for your eyes";
  }

  @Override
  public String toString() {
    // return String.format("%s that %s",super.toString(),getDigs()?"digs":"does not dig");
    String output = super.toString() + " that";
    if (getDigs()){
      output += " digs";
    }else{
      output += " does not dig";
    }
    return output;
  }

  public void setDigs(boolean digs) {
    this.digs = digs;
  }
}
