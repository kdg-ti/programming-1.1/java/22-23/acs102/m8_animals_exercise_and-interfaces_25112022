package animals;

public class AnimalsTestV2 {
  public static void main(String[] args) {
    Animal[] garden = {
        new Dog("Ramses", "Border Collie", "black", 5522),
        new Rabbit("Floppy", "Angora", "gray", true),
        new Dog("Pluto", "Streetdog", "orange", 666)
    };
    String[] names = {"Leonardo", "Donatello","Michelangelo"};
    for (int i = 0; i < garden.length; i++) {
      System.out.println(garden[i]);
      garden[i].setName(names[i]);
     System.out.println(garden[i]);
    }
  }
}
