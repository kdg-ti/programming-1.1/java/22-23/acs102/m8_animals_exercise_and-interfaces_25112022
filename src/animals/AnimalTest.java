package animals;

public class AnimalTest {
  public static void main(String[] args) {
    System.out.printf("Animals: %n %s %n %s %n",
        new Dog("Ramses", "Border Collie","Black",5522),
        new Rabbit("Floppy","Angora", "gray",true)
        );
  }  // main()
}
