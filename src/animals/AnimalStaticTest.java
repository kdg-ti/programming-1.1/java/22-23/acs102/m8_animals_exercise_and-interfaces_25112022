package animals;

import javax.print.Doc;

public class AnimalStaticTest {
  public static void main(String[] args) {

    Dog ramses = new Dog("Ramses", "Border Collie", "Black", 5522);
    System.out.printf("Animals: %n %s %n %s %n",
        ramses,
        new Rabbit("Floppy","Angora", "gray",true)
        );
    Dog scoobydoo = new Dog("ScoobyDoo", "Labrador","Brown");
    System.out.println(scoobydoo);
    System.out.println("The current biggest chipnumber is " + Dog.getBiggestChipNumber());
    System.out.println("Biggest chipnumber between both dogs " + Math.max(ramses.getChipNumber(), scoobydoo.getChipNumber()));
    System.out.println("Pi is "+ Math.PI);
  }  // main()
}
