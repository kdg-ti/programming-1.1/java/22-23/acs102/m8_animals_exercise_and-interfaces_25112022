package animals;

public abstract class Animal {
  private String name;
  private String breed;
  private String colour;

  public Animal(String name, String breed, String colour) {
    this.name = name;
    this.breed = breed;
    this.colour = colour;
  }

  public abstract String getTagLine();

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBreed() {
    return breed;
  }

  public void setBreed(String breed) {
    this.breed = breed;
  }

  public String getColour() {
    return colour;
  }

  public void setColour(String colour) {
    this.colour = colour;
  }

  @Override
  public String toString() {
    return String.format("%s is a %s (%s)",
        getName(),
        getClass().getSimpleName(),
        getTagLine());
  }
}
